import { config, list } from '@keystone-6/core';
import { checkbox, file, relationship, text } from '@keystone-6/core/fields';
import { Lists } from '.keystone/types';

const Post: Lists.Post = list({
  fields: {
    title: text({ validation: { isRequired: true } }),
    slug: text({ isIndexed: 'unique', isFilterable: true }),
    content: text(),
    fotinha: file(),
    boleanoSeChamaCheckbox: checkbox({ isFilterable: true }),
    authors: relationship({ ref: 'User', many: true })
  },
});

export default config({
  db: { provider: 'sqlite', url: 'file:./app.db' },
  experimental: {
    generateNextGraphqlAPI: true,
    generateNodeAPI: true,
  },
  lists: {
    Post,
    User: list({ fields: { name: text() } }),
  },
  files: {
    upload: 'local',
    local: {
      storagePath: 'public/files',
      baseUrl: '/files',
    },
  },
  images: {
    upload: 'local',
    local: {
      storagePath: 'public/images',
      baseUrl: '/images',
    },
  }
});
