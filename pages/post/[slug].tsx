// pages/post/[slug].tsx

import { GetStaticPathsResult, GetStaticPropsContext, InferGetStaticPropsType } from 'next';
import Link from 'next/link';

import { query } from '.keystone/api';
import { Lists } from '.keystone/types';
import Image from 'next/image';

type Post = {
  id: string;
  title: string;
  content: string;
  fotinha?: {
    filename: string;
    filesize: number;
    url: string;
  }
  authors: Array<Author>
};

type Author = {
  id: string
  name: string
}

export default function PostPage({ post }: { post: Post }) {
  return (
    <div>
      <main style={{ margin: '3rem' }}>
        <div>
          <Link href="/">
            <a>&larr; back home</a>
          </Link>
        </div>
        <h1>{post.title}</h1>
        <h4>Quem fez essa porra?</h4>
        <h5>{post.authors.map((a) => <span key={a.id}>id: {a.id} pelego:{a.name}</span>)}</h5>
        <h4>Contenido</h4>
        <p>{post.content}</p>
        {post.fotinha && <p><Image src={post.fotinha.url} layout="intrinsic" width={200} height={300} alt="foda-se"/></p>}
      </main>
    </div>
  );
}

export async function getStaticPaths(): Promise<GetStaticPathsResult> {
  const posts = (await query.Post.findMany({
    query: `slug`,
  })) as { slug: string }[];

  const paths = posts
    .filter(({ slug }) => !!slug)
    .map(({ slug }) => `/post/${slug}`);

  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps({ params }: GetStaticPropsContext) {
  const post = (await query.Post.findOne({
    where: { slug: params!.slug as string },
    query: 'id title content fotinha { filename url filesize } authors { id name }',
  })) as Post | null;
  console.log(post)
  if (!post) {
    return { notFound: true };
  }
  return { props: { post } };
}